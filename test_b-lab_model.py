from b_labs_models import SentenceSegmentator, Tokenizer

segmentator = SentenceSegmentator()
tokenizer = Tokenizer()

gold_tokens =[]
with open('gold_standart.txt', 'r', encoding='utf-8') as gold:
    for line in gold.readlines():
        gold_st = line.split()
        gold_tokens.append(gold_st)

sys_tokens = []
baseline_tokens = []
with open('./test_tokenizer.txt', 'r', encoding='utf-8') as textfile:
    for some_text in textfile.readlines():
            baseline_tokens.append(some_text.split(' '))
            tokens = tokenizer.split(some_text)
            toks = list(tokens)
            sys_tokens.append(toks)
            # print(toks)

# print('--'*50)

my_sys_tokens = []
with open('tokenizer_results.txt', 'r', encoding='utf-8') as my:
    for line in my.readlines():
        if line.startswith('['):
            # print(eval(line.rstrip()))
            my_sys_tokens.append(eval(line.rstrip()))

my_tok_acc, tok_acc, baseline_acc, n = 0, 0, 0, 0
for gold_tok, sys_tok, my, base in zip(gold_tokens, sys_tokens, my_sys_tokens, baseline_tokens):
    good = len((set(gold_tok) & set(sys_tok)))
    tok_acc += good
    n += len(set(gold_tok))

    my_set = len((set(gold_tok) & set(my)))
    my_tok_acc += my_set

    base_set = len((set(gold_tok) & set(base)))
    baseline_acc += base_set

print('b-labs: ', tok_acc/n)
print('mysystem: ', my_tok_acc/n)
print('baseline: ', baseline_acc/n)