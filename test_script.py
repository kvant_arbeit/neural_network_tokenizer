import numpy as np
from keras.models import model_from_json
import pickle
from tokenizer_nn import CharTable
from tokenizer_nn import get_vectorize

SEQ_LEN = 7

with open('./tok_split_models/dataset_params.pickle', 'rb') as f:
    param_file = pickle.load(f)
    CHARS = param_file['chars']
    print(CHARS)

def read_the_results(sentences, normal_predictions):
    with open('tokenizer_results.txt', 'w', encoding='utf-8') as res:
        for sent, pred in zip(sentences, normal_predictions):
            tokens = []
            token = ''
            res.write(sent+'\n')
            for e, (ch, n) in enumerate(zip(sent, pred)):
                l = len(sent)-1
                n = n[0]
                if e == l and n == 1:
                    tokens.append(token.rstrip(' '))
                    token = ch
                    tokens.append(token)
                elif e == l and n == 0:
                    tokens.append(token+ch)
                elif e == 0 and n == 0:
                    token = ch
                elif n == 1:
                    tokens.append(token.rstrip(' '))
                    token = ch
                else:
                    token += ch
            out = tokens if tokens[0] != '' else tokens[1:]
            res.write(str(out)+'\n')

def open_test_file():
    X, sentences = [], []
    with open('./test_tokenizer.txt', 'r', encoding='utf-8') as test:
        sent = test.readlines()
        ctable = CharTable(CHARS)
        for s in sent:
            s = s.rstrip()
            sentences.append(s)
            # s = ['<B>'] + [char for char in s] + ['<E>'] * (SEQ_LEN - 1)
            s = ['<B>'] * (SEQ_LEN // 2) + [char for char in s] + ['<E>'] * (SEQ_LEN // 2)
            n_chars = len(s) + 1
            for i in range(0, n_chars - SEQ_LEN, 1):
                seq_in = s[i:i + SEQ_LEN]
                code = get_vectorize(seq_in, ctable.chars)
                X.append(code)
    print(np.array(X).shape)
    return X, sentences

def main():
    X, sentences = open_test_file()
    json_file = open('./tok_split_models/tokennn_architecture.json', 'r')
    loaded_model_json = json_file.read()
    json_file.close()
    loaded_model = model_from_json(loaded_model_json)
    # load weights into new model
    loaded_model.load_weights("./tok_split_models/1_weights-improvement-12-1.00.hdf5")
    print("Loaded model from disk")
    X_data = np.array(X)
    predictions = loaded_model.predict_classes(X_data)

    cur_pred = predictions
    # привести к формату: [w,o,r,d] [1,0,0,0]
    normal_predictions = []
    for sent in sentences:
        l = len(sent)
        normal_predictions.append(cur_pred[:l])
        cur_pred = cur_pred[l:]

    read_the_results(sentences, normal_predictions)

if __name__ == "__main__":
    main()