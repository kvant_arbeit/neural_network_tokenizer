# neural_rus_tokenizer

### Tokenizer for Russian language

NN was trained on the [OpenCorpora](http://opencorpora.org/?page=downloads) data.

The task of binary classification: neural network decides whether the character is the first character in the token or not.

Input - is the sequence of characters. \<B> - marker of the beginning of the sentence, \<E> - end of sentence.

X -> the slice of sentence, Y -> 0 or 1

For example:

```
<B><B><B>Пример токенизации.<E><E><E>
```
NN works with sequence of length 7:
```
<B><B><B>Прим  1
<B><B>Приме    0
<B>Пример      0
...
```
