import numpy as np
import pickle
from collections import defaultdict
from lxml import etree as et

from numpy import argmax
from keras.callbacks import ModelCheckpoint
from keras.models import Sequential
from keras.layers import Dense, Dropout
from keras.layers import Convolution1D, MaxPooling1D, Embedding, LSTM, Bidirectional

# Tokenizer script for russian
# Splitter text on sentences

MODEL_CHECKPOINT_DIRECTORYNAME = 'tok_split_models'
MODEL_CHECKPOINT_FILENAME = MODEL_CHECKPOINT_DIRECTORYNAME+'/weights.{epoch:02d}-{val_loss:.2f}.hdf5'
MODEL_DATASET_PARAMS_FILENAME = 'dataset_params.pickle'
MODEL_STARTING_CHECKPOINT_FILENAME = 'new_weights.hdf5'
NUMBER_OF_ITERATIONS = 2
BATCH_SIZE = 100
EPOCHS_PER_ITERATION = 20
SEQ_LEN = 7

def lstm_model(dim):
    model = Sequential()

    model.add(Embedding(500, 128, input_length=dim))
    model.add(Convolution1D(128,
                     3,
                     padding='same',
                     activation='relu',
                     strides=1))
    model.add(MaxPooling1D(pool_size=2))
    model.add(Dropout(0.3))
    model.add(Bidirectional(LSTM(128)))
    model.add(Dropout(0.2))
    model.add(Dense(1, activation='sigmoid'))

    print('Compiling...')
    model.compile(loss='binary_crossentropy',
                  optimizer='rmsprop',
                  metrics=['accuracy'])
    model_json = model.to_json()
    with open(MODEL_CHECKPOINT_DIRECTORYNAME + "/tokennn_architecture.json", "w") as json_file:
        json_file.write(model_json)
    return model


def train_nn(X, y, dim):
    """Call for model and train all this stuff"""
    model = lstm_model(dim)
    for iteration in range(1, NUMBER_OF_ITERATIONS):
        print('-' * 50)
        print('Iteration', iteration)
        filepath = MODEL_CHECKPOINT_DIRECTORYNAME + '/' + str(iteration) + "_weights-improvement-{epoch:02d}-{val_acc:.2f}.hdf5"
        checkpoint = ModelCheckpoint(filepath, monitor='val_acc', verbose=1, save_best_only=True, mode='max')
        callbacks_list = [checkpoint]

        model.fit(X, y, validation_split=0.3, batch_size=BATCH_SIZE, epochs=EPOCHS_PER_ITERATION, callbacks=callbacks_list)

    return model


class CharTable(object):

    def __init__(self, chars):
        self.chars = sorted(chars)
        self.char_to_int = dict((c, i) for i, c in enumerate(self.chars))
        self.int_to_char = dict((i, c) for i, c in enumerate(self.chars))

    def encode(self, data):
        """
        Encode data to one-hot
        """
        integer_encoded = [self.char_to_int[char] for char in data]
        # one hot encode
        onehot_encoded = list()
        for value in integer_encoded:
            letter = [0 for _ in range(len(self.chars))]
            letter[value] = 1
            onehot_encoded.append(letter)
        return integer_encoded

    def decode(self, data):
        inverted = self.int_to_char[argmax(data)]
        return inverted

    def save_dataset_params(self):
        params = {'chars': self.chars}
        with open(MODEL_CHECKPOINT_DIRECTORYNAME + '/' + MODEL_DATASET_PARAMS_FILENAME, 'wb') as f:
            pickle.dump(params, f)

def get_vectorize(sequence, chars):
    """
    One sentence of tokens get and vectorize it (encoded)
    :param tokens:
    :return:
    """
    chartable = CharTable(chars)
    encoded_x = chartable.encode(sequence)
    chartable.save_dataset_params()
    return encoded_x

def get_targets(sentence, tokens):
    """
    Catch the targets. If the character is the first letter in token
    :param sequence: array of tokens for one sentence or array of sentences
    :return: array of 0 and 1 for characters
    """
    targets = []
    new_sent = sentence
    for token in tokens:
        first = token[0]
        if first == new_sent[0]:
            targets.extend([1])
        ost = len(token)-1
        targets.extend(ost * [0])
        cut = ost + 1
        try:
            if new_sent[len(token)] == ' ':
                targets.extend([0])
                new_sent = new_sent[len(token)+1:] # +1 space
            else:
                new_sent = new_sent[cut:]
        except:
            break
    if len(targets) == len(sentence):
        targets = targets + [0]*(SEQ_LEN-1)
        return targets, sentence
    else:
        return None, None

def tokenizer_data_prepare(sentences, tokens, chars):
    '''Prepare data for tokenization
    input - characters. need to predict if char is first or not
    binary classification. 0 - is not first, 1 - first
    every SEQ_LEN sequence of chars =>
    :param sentences: array of sentences
    :return X, y: arrays of sentences
    '''
    X, y = [], [] # array of sentences
    for m, sentence in sentences.items():
        sentence = sentence.rstrip()
        targets, sentence = get_targets(sentence, tokens[m])
        if targets:
            # sentence = ['<B>'] + [char for char in sentence]+['<E>']*(SEQ_LEN-1)
            sentence = ['<B>']*(SEQ_LEN//2) + [char for char in sentence]+['<E>']*(SEQ_LEN//2)
            n_chars = len(sentence)+1
            for i, n in zip(range(0, n_chars-SEQ_LEN, 1), targets):
                    seq_in = sentence[i:i + SEQ_LEN]
                    code = get_vectorize(seq_in, chars)
                    # print(code, ''.join(seq_in), n)
                    X.append(code)
                    y.append(n)
        if m % 1000 == 0:
            print(m)
        # if m > 1000:
        #     break
    print(np.array(X).shape)
    print(np.array(y).shape)
    dim = np.array(X).shape[1]
    return X, y, dim

def parse_file():
    '''Parse open_corpora file. It's my training set'''
    ALL_SENTENCES = defaultdict()
    ALL_TOKENS = defaultdict(list)
    with open('./annot.opcorpora.no_ambig.xml', 'r', encoding='utf-8') as f:
        current_etree = et.parse(f)
        sentences = current_etree.findall('//source')
        for n, sent in enumerate(sentences):
                ALL_SENTENCES[n] = sent.text
        current_sentences = current_etree.findall('//tokens')
        for n, sentence in enumerate(current_sentences):
            tokens = []
            for word in sentence.findall('.//token'):
                    tokens.append(word.get('text'))
            ALL_TOKENS[n] = tokens
    print(len(ALL_SENTENCES))
    print(len(ALL_TOKENS))

    return ALL_SENTENCES, ALL_TOKENS

def main():
    all_sentence, all_tokens = parse_file()
    chars = list(set([char for sentences in all_sentence.values() for char in sentences])) + ['<B>', '<E>']
    print(chars)
    X, y, dim = tokenizer_data_prepare(all_sentence, all_tokens, chars)
    train_nn(X, y, dim)

if __name__ == '__main__':
    main()