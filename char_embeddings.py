from collections import defaultdict
from lxml import etree as et
import gensim

from gensim.models.keyedvectors import KeyedVectors

def load_model(model_file):
    model = KeyedVectors.load_word2vec_format(model_file, binary=True, unicode_errors='ignore')
    for item in sorted(model.vocab):
        print(item)
    print(len(model.vocab))

def make_embedding_model(sentences):
    model = gensim.models.Word2Vec(sentences, min_count=1)
    model.wv.save_word2vec_format('./char_model.bin', binary=True)
    return model

def parse_file():
    '''Parse open_corpora file. It's my training set'''
    ALL_SENTENCES = defaultdict()
    ALL_TOKENS = defaultdict(list)
    with open('./annot.opcorpora.no_ambig.xml', 'r', encoding='utf-8') as f:
        current_etree = et.parse(f)
        sentences = current_etree.findall('//source')
        for n, sent in enumerate(sentences):
                ALL_SENTENCES[n] = sent.text
        current_sentences = current_etree.findall('//tokens')
        for n, sentence in enumerate(current_sentences):
            tokens = []
            for word in sentence.findall('.//token'):
                    tokens.append(word.get('text'))
            ALL_TOKENS[n] = tokens
    print(len(ALL_SENTENCES))
    print(len(ALL_TOKENS))

    return ALL_SENTENCES, ALL_TOKENS

def main():
    all_sentence, all_tokens = parse_file()
    chars = list(set([char for sentences in all_sentence.values() for char in sentences]))
    print(chars)
    char_sentences = []
    for sentence in all_sentence.values():
        # chars_ar = [char for char in sentence]
        chars_ar = []
        for char in sentence:
            if char == ' ':
                char = '*'
                chars_ar.append(char)
            else:
                chars_ar.append(char)

        char_sentences.append(chars_ar)
    print(char_sentences[:3])
    make_embedding_model(char_sentences)

if __name__ == '__main__':
    main()
    load_model('char_model.bin')